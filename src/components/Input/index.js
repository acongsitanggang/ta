import React, { useState } from 'react';
import Icon from '../Icon';
import { View, Image, TouchableOpacity, Text, TextInput } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Input = ({ title, ...rest }) => {
  const [secure, setSecure] = useState(true);

  const renderIconEye = () => {
    if (secure == true) {
      return <Icon name="eye" type="ionicons" color="grey" size={23} />;
    } else {
      return <Icon name="eye-with-line" type="entypo" color="grey" size={23} />;
    }
  };

  const handleSecure = () => {
    if (secure == true) {
      setSecure(false);
    } else {
      setSecure(true);
    }
  };
  if (rest.password) {
    return (
      <View>
        {rest.withTitle ?
          <Text style={{ color: rest.grey ? '#878585' : 'black', marginBottom: '2%', fontSize: 15, fontWeight: 'bold' }}>{title}</Text>
          : null}
        <View style={{ backgroundColor: '#F2F2F2', paddingHorizontal: 20, borderRadius: 10, flexDirection: 'row' }}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <TextInput style={{paddingVertical: 15}} {...rest} secureTextEntry={secure} />
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity
              delayPressIn={0}
              onPress={() => handleSecure()}>
              {renderIconEye()}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  } else {
    return (
      <View>
        {rest.withTitle ?
          <Text style={{ color: rest.grey ? '#878585' : 'black', marginBottom: '2%', fontSize: 15, fontWeight: 'bold' }}>{title}</Text>
          : null}
        <TextInput style={{ backgroundColor: '#e6e6e6', paddingVertical: 15, paddingHorizontal: 20, borderRadius: 10 }} {...rest} />
      </View>
    );
  }
};

export default Input;
