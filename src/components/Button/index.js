import React from 'react';
import Icon from '../Icon';
import { View, Image, TouchableOpacity, Text, ActivityIndicator } from 'react-native';
import styles from './styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { globalStyles, WP, HP } from 'assets';

const Button = ({ title, ...rest }) => {
  if (rest.primary) {
    return (
      <TouchableOpacity onPress={!rest.loading ? rest.onPress : null}
        style={{ borderRadius: 10, paddingVertical: 10, width: '100%', alignSelf: 'center', backgroundColor: globalStyles.color.main, alignItems: 'center' }}>
        {!rest.loading ? <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>{title} </Text>
          : <ActivityIndicator
            animating={true}
            size="small"
            color="white"
          />}
      </TouchableOpacity >
    );
  } else if (rest.secondary) {
    return (
      <TouchableOpacity onPress={rest.onPress}
        style={{ borderRadius: 10, paddingVertical: 10, width: '100%', alignSelf: 'center', backgroundColor: 'white', alignItems: 'center' }}>
        <Text style={{ fontSize: 18, fontWeight: 'bold', color: globalStyles.color.main }}>{title} </Text>
      </TouchableOpacity>
    );
  } else if (rest.outline) {
    return (
      <TouchableOpacity onPress={rest.onPress}
        style={{ borderRadius: 10, paddingVertical: 10, width: '100%', alignSelf: 'center', borderColor: 'white', borderWidth: 1, alignItems: 'center' }}>
        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>{title} </Text>
      </TouchableOpacity>
    );
  } else if (rest.outlineRed) {
    return (
      <TouchableOpacity onPress={rest.onPress}
        style={{ borderRadius: 10, paddingVertical: 10, width: '100%', alignSelf: 'center', borderColor: globalStyles.color.main, borderWidth: 1, alignItems: 'center' }}>
        <Text style={{ fontSize: 18, fontWeight: 'bold', color: globalStyles.color.main }}>{title} </Text>
      </TouchableOpacity>
    );
  } 
};

export default Button;
