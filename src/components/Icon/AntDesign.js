import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';

const AntDesign = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default AntDesign;
