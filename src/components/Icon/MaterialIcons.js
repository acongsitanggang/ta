import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

const MaterialIcons = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default MaterialIcons;
