import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

const FontAwesome = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default FontAwesome;
