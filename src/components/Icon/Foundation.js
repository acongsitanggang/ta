import React from 'react';
import Icon from 'react-native-vector-icons/Foundation';

const Foundation = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default Foundation;
