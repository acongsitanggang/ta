import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';

const FontAwesome5 = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default FontAwesome5;
