import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const MaterialCommunityIcons = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default MaterialCommunityIcons;
