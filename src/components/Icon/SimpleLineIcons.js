import React from 'react';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

const SimpleLineIcons = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default SimpleLineIcons;
