import React from 'react';
import Icon from 'react-native-vector-icons/Octicons';

const Octicons = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default Octicons;
