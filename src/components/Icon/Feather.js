import React from 'react';
import Icon from 'react-native-vector-icons/Feather';

const Feather = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default Feather;
