import React from 'react';
import Ionicons from './Ionicons';
import { View } from 'react-native';
import AntDesign from './AntDesign';
import FontAwesome from './FontAwesome';
import FontAwesome5 from './FontAwesome5';
import MaterialCommunityIcons from './MaterialCommunityIcons';
import SimpleLineIcons from './SimpleLineIcons';
import Octicons from './Octicons';
import Feather from './Feather';
import Fontisto from './Fontisto';
import Foundation from './Foundation';
import Entypo from './Entypo';
import MaterialIcons from './MaterialIcons';

const Icon = ({ ...rest }) => {
  if (rest.type === 'ionicons') {
    return <Ionicons {...rest} />;
  }
  if (rest.type === 'antdesign') {
    return <AntDesign {...rest} />;
  }
  if (rest.type === 'entypo') {
    return <Entypo {...rest} />;
  }
  if (rest.type === 'fontawesome') {
    return <FontAwesome {...rest} />;
  }
  if (rest.type === 'fontawesome5') {
    return <FontAwesome5 {...rest} />;
  }
  if (rest.type === 'materialcommunityicons') {
    return <MaterialCommunityIcons {...rest} />;
  }
  if (rest.type === 'simplelineicons') {
    return <SimpleLineIcons {...rest} />;
  }
  if (rest.type === 'octicons') {
    return <Octicons {...rest} />;
  }
  if (rest.type === 'feather') {
    return <Feather {...rest} />;
  }
  if (rest.type === 'fontisto') {
    return <Fontisto {...rest} />;
  }
  if (rest.type === 'foundation') {
    return <Foundation {...rest} />;
  }
  if (rest.type === 'materialicons') {
    return <MaterialIcons {...rest} />;
  }
  return <View />;
};

export default Icon;
