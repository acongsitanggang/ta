import React from 'react';
import Icon from 'react-native-vector-icons/Entypo';

const Entypo = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default Entypo;
