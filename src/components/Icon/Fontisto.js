import React from 'react';
import Icon from 'react-native-vector-icons/Fontisto';

const Fontisto = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default Fontisto;
