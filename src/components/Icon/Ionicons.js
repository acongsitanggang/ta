import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

const Ionicons = ({...rest}) => {
  Icon.loadFont();
  return <Icon {...rest} />;
};

export default Ionicons;
