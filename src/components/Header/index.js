import React from 'react';
import Icon from '../Icon';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import styles from './styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { globalStyles } from '../../assets/globalStyles';

const Header = ({ title, ...rest }) => {
  if (rest.back) {
    return (
      <View style={{ backgroundColor: '#F9F9FB' }}>
        <View
          style={[
            styles.wrapper
          ]}>
          <TouchableOpacity style={styles.backArea} onPress={rest.backTo}>
            <Icon
              name="arrowleft"
              color={rest.color}
              size={wp('8%')}
              type="antdesign"
            />
          </TouchableOpacity>
          <View style={[styles.wrapperTitle, { textAlign: 'left' }]}>
            {rest.dualTitle ?
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold',color:'#C1343A' }}>{rest.title1+' '}</Text>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{rest.title2} </Text>
              </View>
              :
              <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{title} </Text>
            }
          </View>
        </View>
      </View>
    );
  } if (rest.justBack) {
    return (
      <View>
        <TouchableOpacity style={[styles.backArea, { backgroundColor: globalStyles.color.main, alignItems: 'center', justifyContent: 'center' }]} onPress={rest.backTo}>
          <Icon
            name="arrowleft"
            type="antdesign"
            size={wp('6%')}
            color="white"
          />
        </TouchableOpacity>
      </View>
    );
  } else if (rest.round) {
    return (
      <View
        style={[
          styles.wrapper,
          {
            paddingVertical: 5,
            backgroundColor: '#F9F9FB',
            borderRadius: 40,
          },
        ]}>
        <TouchableOpacity style={styles.backArea} onPress={rest.backTo}>
          <Icon
            name="arrowleft"
            type="antdesign"
            size={wp(5)}
            color="white"
          />
        </TouchableOpacity>
        <View style={styles.wrapperTitle}>
          <Text>{title} </Text>
        </View>
      </View>
    );
  } else if (rest.backWithRightIcon) {
    return (
      <View style={styles.wrapper}>
        <TouchableOpacity style={styles.backArea} onPress={rest.leftTo}>
          {/*<Button
            type="icon"
            name="arrowleft"
            color={rest.color}
            size={wp('6%')}
            iconType="antdesign"
          />*/}
        </TouchableOpacity>

        <View style={styles.wrapperTitle}>
          <Text>{title} </Text>
        </View>

        <TouchableOpacity
          style={styles.rightIcon}
          onPress={rest.rightTo}
          delayPressIn={0}>
          {/* <Button
            type="icon"
            name={rest.iconRight.name}
            color={rest.color}
            size={wp('6%')}
            iconType={rest.iconRight.type}
         />*/}
        </TouchableOpacity>
      </View>
    );
  } else if (rest.withRightIcon) {
    return (
      <View style={styles.wrapper}>
        <View style={styles.wrapperTitle}>
          <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{title} </Text>
        </View>
        <View style={{ position: 'absolute', right: '5%' }}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              padding: '1.5%',
            }}
            onPress={rest.rightTo}
            delayPressIn={0}>
            <Icon
              name={rest.iconRight.name}
              type={rest.iconRight.type}
              color={rest.iconRight.color}
              size={wp('6%')}
            />
          </TouchableOpacity>
        </View>

      </View>
    );
  } else if (rest.transparentBack) {
    return (
      <View
        style={[styles.wrapper, { backgroundColor: 'transparent', paddingLeft: '5%' }]}>
        <TouchableOpacity style={{}} onPress={rest.backTo}>
          <Icon
            name="arrowleft"
            color="white"
            size={wp('8%')}
            type="antdesign"
          />
        </TouchableOpacity>
        <View style={{ flex: 1, paddingLeft: '5%' }}>
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white', fontSize: wp('6.5%') }}>{title} </Text>
        </View>
      </View>
    );
  } {
    return (
      <View style={styles.wrapper}>
        <View style={styles.wrapperTitle}>
          <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{title} </Text>
        </View>
      </View>
    );
  }
};

export default Header;
