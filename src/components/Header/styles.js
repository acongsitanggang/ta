import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = {
  wrapper: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 8,
    backgroundColor: '#F9F9FB',
    //borderBottomWidth: 0.5,
    //borderColor: 'lightgrey',
  },
  titleCenter: {
    fontSize: hp('2.3'),
    letterSpacing: 1,
    textAlign: 'center',
    fontWeight:'bold'
  },
  titleLeft: {
    fontSize: hp('2.5%'),
    letterSpacing: 1,
    marginLeft: '2%',
    textAlign: 'left',
  },

  wrapperTitle: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
  },

  title: {
    fontSize: hp('2.5%'),
    letterSpacing: 1,
    marginRight: wp('9%'),
    textAlign: 'center',
  },
  image: {},
  rightIcon: {
    justifyContent: 'center',
    width: wp('8%'),
    paddingVertical: '1.5%',
  },

  backArea: {
    position:'absolute',
    left:'5%',
    borderRadius: 20,
    padding: '1.5%',
    zIndex:1
  },
};

export default styles;
