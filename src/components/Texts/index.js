import React from 'react';
import {Text as RNText} from 'react-native';
import { globalStyles } from "../../assets/globalStyles";

const Texts = ({text, ...rest}) => {
  return (
    <RNText
      style={[{
        fontSize: rest.size,
        fontWeight: rest.bold==null?null:"bold",
        fontStyle: rest.italic==null?null:"italic",
        color: rest.color=='main'?globalStyles.color.main:rest.color,
        marginTop:rest.mrT,
        marginBottom:rest.mrB,
        marginLeft:rest.mrL,
        marginRight:rest.mrR,
        textAlign: rest.align,
        alignSelf:rest.alignSelf,
        textDecorationLine:rest.underline?'underline':null,
      }]}
      {...rest}>
      {text || rest.children}
    </RNText>
  );
};

export default Texts;
