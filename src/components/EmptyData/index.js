import React from "react";
import { View, Image} from "react-native";
import { Texts } from "components";
import { WP } from 'assets';

const EmptyData = () => {
    return (
        <View style={{ alignItems: 'center',flex:1,justifyContent:'center' }}>
            <Image
                style={{ width: WP(30), height: WP(30) }}
                resizeMode="contain"
                source={require('../../assets/images/empty.png')}
            />
            <Texts text="No Data" bold />
        </View>
    )
}

export default EmptyData;