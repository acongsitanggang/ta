import React from 'react';
import { View } from 'react-native';

const Gap = ({ vertical, horizontal }) => {
  return (
    <View style={{ marginTop: vertical, marginLeft: horizontal }} />
  );
};

export default Gap;
