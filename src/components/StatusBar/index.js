import React from "react";
import { View, Platform } from 'react-native';

const StatusBar = () => {
    if (Platform.OS == 'ios') {
        return (
            <View style={{
                height:
                    50, backgroundColor: 'red'
            }} />
        )
    } else {
        return null;
    }

}

export default StatusBar;