
import Input from './Input';
import Texts from './Texts';
import Button from './Button';
import Gap from './Gap';

export { Texts,Input,Button,Gap};
