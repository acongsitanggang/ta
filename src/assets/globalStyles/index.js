import {
  widthPercentageToDP as WP,
  heightPercentageToDP as HP,
} from 'react-native-responsive-screen';

export const globalStyles = {};

globalStyles.color = {
  main: '#1affb2',
  second:'#B01116'
};
