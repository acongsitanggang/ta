import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Main,
  InputData
} from 'pages';

const Stack = createStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Main">
       <Stack.Screen
        name="Main"
        component={Main}
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
      />
       <Stack.Screen
        name="InputData"
        component={InputData}
        options={{
          gestureEnabled: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default Router;
