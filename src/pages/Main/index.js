import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Swiper from 'react-native-web-swiper';
import styles from './styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { globalStyles } from 'assets';

const win = Dimensions.get('window');

const Onboarding = (props) => {
  const { navigation } = props;
  const [index, setIndex] = useState(0);

  return (
    <View style={styles.wrapper}>
      <View style={styles.sectionText}>
        <Swiper
          controlsProps={{
            dotsTouchable: true,
            dotsPos: 'bottom',
            dotActiveStyle: {
              backgroundColor: 'white',
              fontWeight: 'bold',
            },
            prevPos: false,
            nextProps: false,
            nextTitle: '',
            goToNext: () => { },
          }}
          onIndexChanged={(a) => setIndex(a)}
          minDistanceForAction={0.1}>
          <View style={[styles.slideContainer, styles.slideText]}>
            <Image
              source={require('../../assets/images/vest.png')}
              style={{ width: '80%', height: '80%', borderRadius: 10 }}
              resizeMode="cover"
            />
          </View>
          <View style={[styles.slideContainer, styles.slideText]}>
            <Image
              source={require('../../assets/images/smartwatch.png')}
              style={{ width: '80%', height: '80%', borderRadius: 10 }}
              resizeMode="cover"
            />
          </View>
          <View style={[styles.slideContainer, styles.slideText]}>
            <Image
              source={require('../../assets/images/smartband.png')}
              style={{ width: '80%', height: '80%', borderRadius: 10 }}
              resizeMode="cover"
            />
          </View>
        </Swiper>
      </View>
      <TouchableOpacity
        style={{
          backgroundColor: '#1affb2',
          paddingVertical: 10,
          paddingHorizontal: 10,
          borderRadius: 5,
          marginHorizontal:'20%',
          alignItems:'center'
        }}
        onPress={() => navigation.navigate('InputData')}>
        <Text style={{color:'white',fontWeight:'bold'}}>INPUT DATA</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Onboarding;
