import {Dimensions, Platform} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const DeviceWidth = Dimensions.get('window').width;

const styles = {
  // Wrapper
  wrapper: {
    flex: 1,
    //backgroundColor: 'green',
    paddingTop: '10%',
  },

  // Section Bottom
  sectionBottom: {
    flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },

  //Section Text
  sectionText: {
    flex: .8,
    paddingBottom: '10%',
  },

  // Section Button
  sectionButton: {
    flex: 0.15,
    justifyContent: 'center',
    alignItems: 'center',
  },

  // Grid Button
  gridButton: {
    paddingHorizontal: '6%',
  },

  // Col Button
  colButton: {
    paddingHorizontal: '2%',
  },

  // Slide Container
  slideContainer: {
    flex: 0.8,
    justifyContent: 'center',
    marginHorizontal:'5%',
    borderRadius:10,
    alignItems:'center',
    justifyContent:'center'
  },

  // Slide Text Big
  slideTextBig: {
    fontWeight: 'bold',
    fontSize: wp('9%'),
  },

  // Section Text Small
  sectionTextSmall: {
    marginTop: '5%',
    alignItems: 'center',
  },

  // Slide Text Small
  slideTextSmall: {
    fontSize: wp('4%'),
    color: 'red',
  },

  // Button
  button: {
    backgroundColor:'red',
    alignItems: 'center',
    paddingVertical: '3%',
    borderRadius: 10,
  },
  // Button
  buttonDisable: {
    backgroundColor: '#6fa658',
    alignItems: 'center',
    paddingVertical: '3%',
    borderRadius: 10,
  },
};

export default styles;
