import {globalStyle} from 'assets';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = {
  // Wrapper
  wrapper: {
    flex: 1,
    backgroundColor: 'white',
    //paddingTop:Platform.OS=='ios'?40:null,
  },

  // Section Main
  sectionMain: {
    flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    paddingTop: '5%',
    justifyContent:'center'
  },

  // Input Area
  inputArea:{
    borderWidth:1,
    borderColor:'grey',
    marginBottom:'5%',
    padding:'2%',
    fontSize:12,
    minHeight:80,
    textAlignVertical: 'top',
  }


};

export default styles;
