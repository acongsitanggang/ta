import React, { useState, useEffect } from "react";
import { Alert, Text, SafeAreaView, ScrollView, NativeModules } from "react-native";
import { Texts, Input, Button, Gap } from "components";
import styles from "./styles";
import axios from "axios";
import CryptoJS from "react-native-crypto-js";

const Page = (props) => {
    const { navigation } = props;
    const [loading, setLoading] = useState(false);

    const [detakJantung, setDetakJantung] = useState('');

    const encrypt = (text) => {
        const data = text;
        const iv = 'AAAAAAAAAAAAAAAA';
        const key = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';

        const fkey = CryptoJS.enc.Utf8.parse(key);
        const fiv = CryptoJS.enc.Utf8.parse(iv);

        const enc = CryptoJS.AES.encrypt(data, fkey, {
            iv: fiv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
        });

        const final = enc.ciphertext.toString(CryptoJS.enc.Base64);
        return final;
    }

    const decrypt = (text) => {
        let bytes = CryptoJS.AES.decrypt(text, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
        let originalText = bytes.toString(CryptoJS.enc.Utf8);
        return originalText;
    }

    const onSubmit = () => {
        if (detakJantung == '') {
            Alert.alert('Detak Jantung Masih Kosong');
        } else {
            setLoading(true);
            axios({
                method: 'post',
                url: 'http://192.168.86.192/ta_encryption/add.php',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: {
                    detak_jantung: encrypt(detakJantung)
                }
            }).then(function (response) {
                if (response.data.success) {
                    Alert.alert('Data Berhasil Diinput');
                }
                console.log(response.data);
                navigation.goBack();
                setLoading(false);
            })
                .catch(function (error) {
                    console.log(error)
                    Alert.alert('Something Wrong');
                    setLoading(false);
                });
        }
        console.log(encrypt(detakJantung));
      
    }

    const onSubmit1 = () => {
        console.log(decrypt('FSfhJ/gk3iEJOPVLyFVc2Q=='))
    }

    return (
        <SafeAreaView style={styles.page}>
            <ScrollView style={{ paddingHorizontal: "5%", paddingTop: '5%' }}>
                <Input
                    title="Detak Jantung"
                    withTitle
                    value={detakJantung}
                    placeholder="Detak Jantung"
                    onChangeText={(text) => setDetakJantung(text)}
                />
                <Gap vertical={10} />
                <Button
                    primary
                    title="Input"
                    borderRadius={40}
                    loading={loading}
                    onPress={() => onSubmit()}
                />
            </ScrollView>
        </SafeAreaView>
    );
};



export default Page;