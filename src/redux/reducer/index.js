import {combineReducers} from 'redux';
import userDataReducer from '../list/reduxReducer';

const reducer = combineReducers({
  userDataReducer,
});

export default reducer;
